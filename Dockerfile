FROM alpine:latest AS builder

ARG ALPINE_VERSION=3.20
ARG GITREPO_URL=https://gitlab.mim-libre.fr/infrabricks/builders/docker-containers/alpinelinux/alpine-make-rootfs.git

RUN apk add --no-cache --update git bash

WORKDIR /src
RUN mkdir /src/rootfs && \
    git clone $GITREPO_URL && \
    cd alpine-make-rootfs && \
    /bin/bash ./alpine-make-rootfs --branch v$ALPINE_VERSION \
    --packages 'apk-tools' /src/rootfs

FROM scratch

LABEL maintainer="PCLL Team"

WORKDIR /
COPY --from=builder /src/rootfs .
RUN apk upgrade --available --no-cache
CMD ["/bin/sh"]
