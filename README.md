# Alpine container builder

This dockerfile builds a minimal Alpine container with the help of "Alpine make rootfs" script.

Build command example

```
docker build -t local/alpine:3.20 --build-arg="ALPINE_VERSION=3.20" --progress=plain .
```
